const express=require("express");
const router=express.Router();
//importamos el Schema
const usuarioSchema=require("../models/usuario.models");
// crear un nuevo  usuario
router.post('/usuario',(req,res)=>{
    const usuario=usuarioSchema(req.body);
    usuario
    .save()
    .then((data)=>res.json(data))
    .catch((error)=>res.json({message: error}));
});

// obtener todos los usuarios GET
router.get('/usuario',(req,res)=>{
    usuarioSchema
    .find()
    .then((data)=>res.json(data))
    .catch((error)=>res.json({message: error}));
});

// Buscar usuarios por id
router.get('/usuario/:id',(req,res)=>{
    const{id}=req.params;
    usuarioSchema
    .findById(id)
    .then((data)=>res.json(data))
    .catch((error)=>res.json({message: error}));
});

// Actualizar un  usuario por id
router.put('/usuario/:id',(req,res)=>{
    const{id}=req.params;
    const{nickusuario,clave, tipousuario,nombres,apellidos,tipodoc,numdoc,direccion, telefono,email,estado}=req.body;
    usuarioSchema
    .updateOne({_id:id},{$set:{nickusuario,clave, tipousuario,nombres,apellidos,tipodoc,numdoc,direccion, telefono,email,estado}})
    .then((data)=>res.json(data))
    .catch((error)=>res.json({message: error}));
});


// Eliminar un  usuario por id
router.delete('/usuario/:id',(req,res)=>{
    const{id}=req.params;
    usuarioSchema
    .remove({_id:id})
    .then((data)=>res.json(data))
    .catch((error)=>res.json({message: error}));
});

module.exports=router;