const express=require("express");
const router=express.Router();
//importamos el Schema
const productoSchema=require("../models/producto.models");


// crear un nuevo  producto
router.post('/producto',(req,res)=>{
    const producto=productoSchema(req.body);
    producto
    .save()
    .then((data)=>res.json(data))
    .catch((error)=>res.json({message: error}));
});



// obtener todos los usuarios GET
router.get('/producto',(req,res)=>{
    productoSchema
    .find()
    .then((data)=>res.json(data))
    .catch((error)=>res.json({message: error}));
});


// Buscar Productos por id
router.get('/producto/:id',(req,res)=>{
    const{id}=req.params;
    productoSchema
    .findById(id)
    .then((data)=>res.json(data))
    .catch((error)=>res.json({message: error}));
});


// Actualizar un  Producto por id
router.put('/producto/:id',(req,res)=>{
    const{id}=req.params;
    const{nombre,unidadmedida,valorcompra,cantidad,categoria,codigo,valorventa}=req.body;
    productoSchema
    .updateOne({_id:id},{$set:{nombre,unidadmedida,valorcompra,cantidad,categoria,codigo,valorventa}})
    .then((data)=>res.json(data))
    .catch((error)=>res.json({message: error}));
});


// Eliminar un  Producto por id
router.delete('/producto/:id',(req,res)=>{
    const{id}=req.params;
    productoSchema
    .remove({_id:id})
    .then((data)=>res.json(data))
    .catch((error)=>res.json({message: error}));
});

module.exports=router;