const mongoose=require("mongoose");

const productoSchema=mongoose.Schema({
    nombre: {
        type: String,
        required: true,
      },
      unidadmedida: {
        type: String,
      },
      valorcompra: {
        type: Number,
        required: true,
      },
      cantidad: {
        type: Number,
        required: true,
      },
      categoria: {
        type: String,
      },
      codigo: {
        type: String,
      },
      valorventa: {
        type: Number,
      }

});

module.exports=mongoose.model('productos',productoSchema);