const mongoose=require("mongoose");

const usuarioSchema=mongoose.Schema({
    nickusuario:{
        type: String,
        required:true,
    },
    clave:{
        type: String,
        required:true
    },
    tipousuario:{
        type: String,
        required:true
    },
    nombres: {
        type: String,
        required: true,
    },
    apellidos: {
        type: String,
        required: true,
    },
    tipodoc: {
        type: String,
        required: true,
    },
    numdoc: {
        type: String,
        required: true,
    },
    direccion: {
        type: String,
        required: true,
    },
    telefono: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    estado: {
        type: String,
        required: true
    }

});

module.exports = mongoose.model('usuarios',usuarioSchema);