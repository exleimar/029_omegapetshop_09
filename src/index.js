const express= require("express");
const mongoose=require("mongoose");

require("dotenv").config();//libreria para variables customizadas
const productoRoutes=require("./routes/producto.route");
const usuarioRoutes=require("./routes/usuario.route");
const app=express();
const puerto=process.env.PORT || 5000;

//middleware
app.use(express.json());
app.use('/omegapetshop',productoRoutes);
app.use('/omegapetshop',usuarioRoutes);




//routes
app.get('/',(req,res)=>{
    res.send("Bienvenido a la tienda de mascotas");
});
//conexion a la bd de mongoAtlas
mongoose.connect(process.env.URI_MONGO).then(()=>console.log("Conexion Exitosa.."))
.catch((error)=>console.error(error));

app.listen(puerto,()=>
console.log('servidor corriendo en el puerto',puerto));


